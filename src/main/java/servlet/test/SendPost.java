package servlet.test;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;
import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class SendPost {
	
	private HttpPost httpPost;
	private String url;
	private Integer httpCode;
	private String xlmFileName;
	


	public SendPost(String url) {
		
		this.url = url;
		this.httpPost = new HttpPost(url);
		this.httpCode = -9999;
		this.xlmFileName = "-9999";
		
	}
	
	public String sendXmlPost(String xml) throws ClientProtocolException, IOException {
		
		String s = new String();
		
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            
        	httpPost.setHeader("Content-Type", "application/xml");
            httpPost.setEntity(new StringEntity(xml));

//            System.out.println("Richiesta in elaborazione:\n" + httpPost.getRequestLine());

            CloseableHttpResponse response = httpClient.execute(httpPost);
            
            HttpEntity entity = response.getEntity();
            
            s = EntityUtils.toString(entity, "UTF-8");
            this.httpCode = response.getStatusLine().getStatusCode();
           
            EntityUtils.consume(entity);         
//            System.out.println(s);         
        }  

   
        return s;
		
	}
	
	  
	public static String loadXml (String file) throws FileNotFoundException {
		
		InputStream in = new FileInputStream(file);
		String result = new BufferedReader(new InputStreamReader(in)).lines().collect(Collectors.joining("\n"));
		
		return result;
		
	}
	
	
	public HttpPost getHttpPost() {
		return httpPost;
	}

	public void setHttpPost(HttpPost httpPost) {
		this.httpPost = httpPost;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(Integer httpCode) {
		this.httpCode = httpCode;
	}

	public String getXlmFileName() {
		return xlmFileName;
	}

	public void setXlmFileName(String xlmFileName) {
		this.xlmFileName = xlmFileName;
	}
	

}
