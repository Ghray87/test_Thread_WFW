package servlet.test;


public class XmlFile {

	private String name;
	private String xml;
	private String resultCode;
	
	public XmlFile(String name, String xml) {
		
		this.name = name;
		this.xml = xml;
		resultCode = "Initialization";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
}
