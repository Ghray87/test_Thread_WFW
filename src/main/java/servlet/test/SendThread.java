package servlet.test;

import java.io.IOException;
import java.net.SocketException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Vector;

public class SendThread implements Runnable{
	
	private SendPost post;
	private ArrayList<XmlFile> xmlList;
	private String idTab;
	private Integer idThread;
	private Integer delay;
	public static String SEPARATOR = " ";
	
	public SendThread(SendPost post, Integer idThread, String idTab, ArrayList<XmlFile> xmlList, Integer delay) {
		
		this.post = post;
		this.xmlList = xmlList;
		this.idTab = idTab;
		this.idThread = idThread;	
		this.delay = delay;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			
			Vector<Long> durations = new Vector<>();
			Vector<Integer> lengthCodes = new Vector<>();
			Vector<String> codes = new Vector<>();
			
			for(XmlFile xmlFile : xmlList) {
				
				long start = System.currentTimeMillis();
					try {
						String replyXml = post.sendXmlPost(xmlFile.getXml());
	//					System.out.println(replyXml);
						Integer l = replyXml.length();
						lengthCodes.add(l);
						String code = post.getHttpCode().toString();
						xmlFile.setResultCode(code);
						
						if(!code.equals("200")) {
							System.out.println(replyXml);
						}
						codes.add(xmlFile.getResultCode());
					} catch (Exception e) {				
						Integer l = 0;
						lengthCodes.add(l);
						xmlFile.setResultCode("-9999");
						codes.add(xmlFile.getResultCode());
						e.printStackTrace();
						}
				long duration = System.currentTimeMillis() - start;	
	
				durations.add(duration);
				
							
			}
			
				
			LocalDateTime startTime = LocalDateTime.now();
			
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
	
	        String time = startTime.format(formatter);
	        
			String output = new StringBuilder().append(time)
											   .append(SEPARATOR)
											   .append(this.idThread)
											   .append(SEPARATOR)
											   .append(this.idTab)
											   .append(SEPARATOR)
											   .append(durations.get(0))
											   .append(SEPARATOR)
											   .append(codes.get(0))
											   .append(SEPARATOR)
											   .append(lengthCodes.get(0))
											   .append(SEPARATOR)
											   .append(durations.get(1))
											   .append(SEPARATOR)
											   .append(codes.get(1))
											   .append(SEPARATOR)
											   .append(lengthCodes.get(1))
											   .append(SEPARATOR)
											   .append(delay)
											   .toString();
			System.out.println(output);
		
		} finally {
		    post.getHttpPost().releaseConnection();            
		}

	}

	public SendPost getPost() {
		return post;
	}

	public void setPost(SendPost post) {
		this.post = post;
	}

	public ArrayList<XmlFile> getXmlList() {
		return xmlList;
	}

	public void setXmlList(ArrayList<XmlFile> xmlList) {
		this.xmlList = xmlList;
	}

	public String getIdTab() {
		return idTab;
	}

	public void setIdTab(String idTab) {
		this.idTab = idTab;
	}

	public Integer getIdThread() {
		return idThread;
	}

	public void setIdThread(Integer idThread) {
		this.idThread = idThread;
	}


	
	
}